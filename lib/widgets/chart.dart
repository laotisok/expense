import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'chart_bar.dart';
import '../models/transaction.dart';

class Chart extends StatelessWidget {
  final List<Transaction> recentTransactions;

  Chart(this.recentTransactions);

  List<Map<String, Object>> get groupTransactionValues {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(
        Duration(days: index),
      );
      double totalSum = 0.0;

      for (var i = 0; i < recentTransactions.length; i++) {
        if (recentTransactions[i].date.day == weekDay.day &&
            recentTransactions[i].date.month == weekDay.month &&
            recentTransactions[i].date.year == weekDay.year) {
          totalSum += recentTransactions[i].amount;
        }
      }
      print(DateFormat.E().format(weekDay));
      return {
        'day': DateFormat.E().format(weekDay).substring(0, 1),
        'amount': totalSum,
      };
      //reversed bar
    }).reversed.toList();
  }

  double get totalSpending {
    //fold allow to list to another type with certain logic
    return groupTransactionValues.fold(0.0, (sum, item) {
      return sum + item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      //dropshadow
      elevation: 6,
      margin: EdgeInsets.all(20),
      //Padding here is at if u want just to add padding
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Row(
          //make bar have more spaces
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: groupTransactionValues.map((data) {
            //Flexible add this widget to make fit function available
            return Flexible(
              //flex is width of Row, default value is 1
              flex: 1,
              //FlexFit restrict size, FlexFit.tight take all the available space
              //to replace FlexFit.tight we can use Expanded widget
              fit: FlexFit.tight,
              child: ChartBar(
                  data['day'],
                  data['amount'],
                  totalSpending == 0.0
                      ? 0.0
                      : (data['amount'] as double) / totalSpending),
            ); //Text('${data['day']}: ${data['amount']}');
          }).toList(),
        ),
      ),
    );
  }
}
