import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
  final String label;
  final double spendingAmount;
  final double spendingPctOfTotal;

  ChartBar(this.label, this.spendingAmount, this.spendingPctOfTotal);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      //use LayoutBuilder to have builder function and get the constraints, and use constraints to get specific height of widget
      builder: (ctx, constraints) {
        return Column(
          children: [
            //FittedBox make the text shring
            //toStringAsFixed to remove decimal
            Container(
              // height: 20,
              height: constraints.maxHeight * 0.15,
              child: FittedBox(
                child: Text('\$${spendingAmount.toStringAsFixed(0)}'),
              ),
            ),
            SizedBox(
              // height: 4,
              height: constraints.maxHeight * 0.05,
            ),
            Container(
              // height: 60,
              height: constraints.maxHeight * 0.6,
              width: 10,
              //Stack widget allow to place element to each other (Overlap each other)
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 1.0),
                      color: Color.fromRGBO(220, 220, 220, 1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  //FractionallySizedBox allow to create a box with fraction size
                  FractionallySizedBox(
                    //spendingPctOfTotal expect to value 0 and 1, 1 mean full hight of parent container, 0 will be 0
                    heightFactor: spendingPctOfTotal,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              // height: 4,
              height: constraints.maxHeight * 0.05,
            ),
            Container(
              height: constraints.maxHeight * 0.15,
              //FittedBox to make text resize to fit in the box
              child: FittedBox(
                child: Text(label),
              ),
            ),
          ],
        );
      },
    );
  }
}
