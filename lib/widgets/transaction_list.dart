import 'package:DailyExpenses/widgets/transaction_item.dart';
import 'package:flutter/material.dart';
import '../models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTx;

  TransactionList(this.transactions, this.deleteTx);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height * 0.6, //0.6 mean 60% of screen
      //ListView is default with column and singleChildScrollview
      //ListView render all data, but ListView.builder render only show data on screen
      child: transactions.isEmpty
          ? LayoutBuilder(builder: (ctx, constraints) {
              return Column(
                children: [
                  Text(
                    "No Transactions added yet!",
                    style: Theme.of(context).textTheme.title,
                  ),
                  //SizedBox mostly use for provide space
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    // height: 200,
                    height: constraints.maxHeight * 0.6,
                    child: Image.asset(
                      'assets/images/waiting.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              );
            })
          : ListView(
              children:
                  // .builder(
                  // //ListView.builder need this function to render data
                  // itemBuilder: (ctx, index)

                  //we use transactions instead of ListView because we want to use key, to make it remember state, by default we shouldn't use Key
                  transactions
                      .map((tx) => TransactionItem(
                            //key make item rember their own state
                            key: ValueKey(tx.id),
                            transaction: tx,
                            deleteTx: deleteTx,
                          ))
                      .toList(),

              //*Card show list of item v1.0
              // return Card(
              //   child: Row(
              //     children: [
              //       Container(
              //         margin: EdgeInsets.symmetric(
              //           vertical: 10,
              //           horizontal: 15,
              //         ),
              //         //not all the widget has decoration, but Container has
              //         decoration: BoxDecoration(
              //           border: Border.all(
              //             //this color use from theme
              //             color:
              //                 Theme.of(context).primaryColor, //Colors.purple,
              //             width: 2,
              //           ),
              //         ),
              //         padding: EdgeInsets.all(10),
              //         child: Text(
              //             //toStringAsFixed(2) format number have 2 number behind ex: 1.00
              //             '\$${transactions[index].amount.toStringAsFixed(2)}', //+ tx.amount.toString(),
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 20,
              //               color: Theme.of(context)
              //                   .primaryColor, //Colors.purple,
              //             )),
              //       ),
              //       Column(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           Text(
              //             transactions[index].title,
              //             // style: TextStyle(
              //             //   fontSize: 16,
              //             //   fontWeight: FontWeight.bold,
              //             // ),
              //             //this use style that set in main file "textTheme"
              //             style: Theme.of(context).textTheme.title,
              //           ),
              //           Text(
              //               //DateFormat('yyyy-MM-dd')
              //               DateFormat.yMMMMd()
              //                   .format(transactions[index].date),
              //               style: TextStyle(
              //                 color: Colors.grey,
              //               ))
              //         ],
              //       )
              //     ],
              //   ),
              // );

              // },
              // //tell itemBuilder total value
              // itemCount: transactions.length,

              // children: transactions.map((tx) {
              // }).toList(),
            ),
    );
  }
}
