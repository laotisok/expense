import 'dart:io';

import 'package:DailyExpenses/widgets/adaptive_flat_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

//this class use StatefulWidget because avoid text field can't change
class NewTransaction extends StatefulWidget {
  //value of textField
  final Function addTx;

  NewTransaction(this.addTx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final _titleController = TextEditingController();

  final _amountController = TextEditingController();

  DateTime _selectedDate;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didUpdateWidget(covariant NewTransaction oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void _submitData() {
    if (_amountController.text.isEmpty) {
      return;
    }
    final enterdTitle = _titleController.text;
    final enteredAmount = double.parse(_amountController.text);

    if (enterdTitle.isEmpty || enteredAmount <= 0 || _selectedDate == null) {
      return;
    }

    //widget make us able to access widget class above
    widget.addTx(
      enterdTitle,
      enteredAmount,
      _selectedDate,
    );

    //this close latest modal
    Navigator.of(context).pop();
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2021),
      lastDate: DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    //SingleChildScrollView make it scroll able
    return SingleChildScrollView(
      child: Card(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            //To move text field up when open keyboard
            bottom: MediaQuery.of(context).viewInsets.bottom + 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextField(
                decoration: InputDecoration(
                  labelText: 'Title',
                ),
                //controller is value of textField
                controller: _titleController,
                onSubmitted: (_) => _submitData,
                // onChanged: (val) {
                //   titleInput = val;
                // },
              ),
              TextField(
                decoration: InputDecoration(
                  labelText: 'Amount',
                ),
                controller: _amountController,
                keyboardType: TextInputType.number,
                //submitData doesn't have () because we just want to pointer on it, not call.
                //make annonymouse arrow function just to make flutter not complain about syntax. but we don't use it.
                onSubmitted: (_) => _submitData,
                // onChanged: (val) => amountInput = val,
              ),
              Container(
                height: 70,
                child: Row(
                  children: [
                    //Expanded take all the free space of row
                    Expanded(
                      child: Text(
                        _selectedDate == null
                            ? 'No Date Chosen!'
                            : 'Picked Date: ${DateFormat.yMd().format(_selectedDate)}',
                      ),
                    ),

                    // use AdaptiveFlatButton instead
                    // Platform.isIOS
                    //     ? CupertinoButton(
                    //         child: Text(
                    //           'Choose Date',
                    //           style: TextStyle(
                    //             fontWeight: FontWeight.bold,
                    //           ),
                    //         ),
                    //         //without parenthesis mean just want to reference
                    //         onPressed: _presentDatePicker,
                    //       )
                    //     : FlatButton(
                    //         textColor: Theme.of(context).primaryColor,
                    //         child: Text(
                    //           'Choose Date',
                    //           style: TextStyle(
                    //             fontWeight: FontWeight.bold,
                    //           ),
                    //         ),
                    //         //without parenthesis mean just want to reference
                    //         onPressed: _presentDatePicker,
                    //       ),

                    AdaptiveFlatButton('Choose Date', _presentDatePicker),
                  ],
                ),
              ),
              RaisedButton(
                child: Text('Add Transaction'),
                color: Theme.of(context).primaryColor,
                textColor: Theme.of(context).textTheme.button.color,
                onPressed: _submitData,
                // () {
                //   addTx(
                //     titleController.text,
                //     double.parse(amountController.text),
                //   );
                // },
              )
            ],
          ),
        ),
      ),
    );
  }
}
