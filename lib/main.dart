import 'dart:io'; //core feature of dart
import 'package:DailyExpenses/models/transaction.dart';
import 'package:DailyExpenses/widgets/chart.dart';
import 'package:DailyExpenses/widgets/new_transaction.dart';
import 'package:DailyExpenses/widgets/transaction_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './widgets/chart.dart';

void main() {
  //This make it can't rotate
  // WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setPreferredOrientations([
  //   DeviceOrientation.portraitUp,
  //   DeviceOrientation.portraitDown,
  // ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Personal Expenses', //Name of the app
      //global of style
      theme: ThemeData(
        //this generate different color automatic base on primary color
        primarySwatch: Colors.purple,
        //this mix color, it will configure color which to use automaticly
        accentColor: Colors.amber,
        //errorColor: Colors.red, //default is red
        fontFamily: 'OpenSans',
        //style for the rest of app
        textTheme: ThemeData.light().textTheme.copyWith(
              title: TextStyle(
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
              button: TextStyle(color: Colors.white),
            ),
        //style for appbar
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
                title: TextStyle(
                  fontFamily: 'OpenSans',
                  fontSize: 20,
                ),
              ),
        ),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  // final List<Transaction> transactions = [
  //   Transaction(
  //     id: 't1',
  //     title: 'New Shoes',
  //     amount: 69.99,
  //     date: DateTime.now(),
  //   ),
  //   Transaction(
  //     id: 't1',
  //     title: 'Weekly Groceries',
  //     amount: 16.53,
  //     date: DateTime.now(),
  //   ),
  // ];

  // String titleInput;
  // String amountInput;
  // final titleController = TextEditingController();
  // final amountController = TextEditingController();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

// if want to extends multiple class should use "with" keyword, example extend from WidgetsBindingObserver
class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  final List<Transaction> _userTransaction = [
    // Transaction(
    //   id: 't1',
    //   title: 'New Shoes',
    //   amount: 69.99,
    //   date: DateTime.now(),
    // ),
    // Transaction(
    //   id: 't1',
    //   title: 'Weekly Groceries',
    //   amount: 16.53,
    //   date: DateTime.now(),
    // ),
  ];

  bool _showChart = false;

  @override
  void initState() {
    //Listener
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  didChchangeAppLifecycleState(AppLifecycleState state) {}

  @override
  dispose() {
    //clear life cycle listener to avoid memory leak
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  //Only transaction younger than 7 days are include here.
  List<Transaction> get _recentTransactions {
    return _userTransaction.where((tx) {
      return tx.date.isAfter(
        //subtract return a new date with duration
        DateTime.now().subtract(
          Duration(days: 7),
        ),
      );
    }).toList();
  }

  void _addNewTransaction(
    String txTitle,
    double txAmount,
    DateTime chosenDate,
  ) {
    final newTx = Transaction(
      id: DateTime.now().toString(),
      title: txTitle,
      amount: txAmount,
      date: chosenDate,
    );

    //after _startAddNewTransaction add data to this _userTransaction list.
    setState(() {
      _userTransaction.add(newTx);
    });
  }

  void _startAddNewTransaction(BuildContext ctx) {
    //create modal
    showModalBottomSheet(
      context: ctx,
      builder: (_) {
        return GestureDetector(
          //GestureDetector onTap do nothing to avoid tab on the sheet itself close the modal
          onTap: () {},
          child: NewTransaction(_addNewTransaction),
          //this catch the tap event
          behavior: HitTestBehavior.opaque,
        );
      },
    );
  }

  void _deleteTransaction(String id) {
    setState(() {
      _userTransaction.removeWhere((tx) {
        return tx.id == id;
      });
    });
  }

  List<Widget> _buildLandscapeContent(
      MediaQueryData mediaQuery, AppBar appBar, Widget txListWidget) {
    return [
      Row(
        //follow main to make it center
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Show Chart",
            style: Theme.of(context).textTheme.title,
          ),
          //adaptive here is optional, but if use it will adapt to device if android it will render switch of android, but if ios it will render switch of ios.
          Switch.adaptive(
            activeColor: Theme.of(context).accentColor,
            value: _showChart,
            onChanged: (val) {
              setState(() {
                _showChart = val;
              });
            },
          ),
        ],
      ),
      _showChart
          ? Container(
              height: (mediaQuery.size.height -
                      appBar.preferredSize.height -
                      mediaQuery.padding.top) *
                  0.7,
              child: Chart(_recentTransactions),
            )
          : txListWidget
    ];
  }

  List<Widget> _buildPortraitContent(
      MediaQueryData mediaQuery, AppBar appBar, Widget txListWidget) {
    return [
      Container(
        height: (mediaQuery.size.height -
                appBar.preferredSize.height -
                mediaQuery.padding.top) *
            0.3,
        child: Chart(_recentTransactions),
      ),
      txListWidget
    ];
  }

  //build function call when: setState, MediaQuery or Theme change
  //every widget have their own context, context is widget information (Meta Information)
  @override
  Widget build(BuildContext context) {
    //store AppBar so to make it available everywhere, to get its information in other file
    //appBar need specific type to make it work with CupertinoNavigationBar
    final PreferredSizeWidget appBar = Platform.isIOS
        ? CupertinoNavigationBar(
            middle: Text(
              'Personal Expenses',
              //style: TextStyle(fontFamily: 'OpenSans'),
            ),
            //Row in cupertino mainAxisSize is max so it will take all with of screen and will overflow. that's why we need to se MainAxisSize to min
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                //cupertino doesn't have IconButton widget like material.dart
                GestureDetector(
                  child: Icon(CupertinoIcons.add),
                  onTap: () => _startAddNewTransaction(context),
                )
              ],
            ),
          )
        : AppBar(
            title: Text(
              'Personal Expenses',
              //style: TextStyle(fontFamily: 'OpenSans'),
            ),
            actions: [
              IconButton(
                icon: Icon(Icons.add),
                onPressed: () => _startAddNewTransaction(context),
              ),
            ],
          );

    //check rotation of phone
    // use MediaQuery to get the current device information
    final isLandscape =
        MediaQuery.of(context).orientation == Orientation.landscape;

    final mediaQuery = MediaQuery.of(context);

    final txListWidget = Container(
      height: (mediaQuery.size.height -
              appBar.preferredSize.height -
              mediaQuery.padding.top) *
          0.7,
      child: TransactionList(_userTransaction, _deleteTransaction),
    );

    //SafeArea make it respect other area, not to make it overload at each other
    final pageBody = SafeArea(
      child: SingleChildScrollView(
        //SingleChildScrollView make it able to scroll
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start, //this is default
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              // Container(
              //   width: double.infinity,
              //   child: Card(
              //     color: Colors.blue,
              //     child: Text('CHART!'),
              //     elevation: 5,
              //   ),
              // ),

              // Card(
              //   color: Colors.red,
              //   child: Text('LIST OF TX'),
              // ),

              //if isLandscape true show swich button
              if (isLandscape)
                ..._buildLandscapeContent(mediaQuery, appBar, txListWidget),

              //show all if it not landscape
              //... mean add all element of _buildPortraitContent list widget to parent list widget
              if (!isLandscape)
                ..._buildPortraitContent(mediaQuery, appBar, txListWidget),
              //if (!isLandscape) txListWidget,

              //If it landscape need to check condition before show
              if (isLandscape)
                ..._buildLandscapeContent(mediaQuery, appBar, txListWidget),
              //   |
              //   V  this _buildLandscapeContent(mediaQuery, appBar, txListWidget) the same here
              // if (isLandscape)
              //   _showChart
              //       ? Container(
              //           height: (mediaQuery.size.height -
              //                   appBar.preferredSize.height -
              //                   mediaQuery.padding.top) *
              //               0.7,
              //           child: Chart(_recentTransactions),
              //         )
              //       : txListWidget
            ]),
      ),
    );

    //Scaffold default give us a meterial design widget
    //CupertinoPageScaffold ios style
    return Platform.isIOS
        ? CupertinoPageScaffold(
            navigationBar: appBar,
            child: pageBody,
          )
        : Scaffold(
            appBar: appBar,
            body: pageBody,
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            //check condition because don't want to render floating button in ios
            floatingActionButton: Platform.isIOS
                ? Container()
                : FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () => _startAddNewTransaction(context),
                  ),
          );
  }
}
